package seotests;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import utils.GetProperties;
import utils.Screenshot;
import utils.TestUtils;

public class SeoTest {
	Map<String, String> properties;
	WebDriver driver;
	
	@BeforeClass
	private void setUp() {
		properties=new HashMap<String, String>();
		properties = GetProperties.readProperties("login.properties");
		launchBrowser();
	}
	
	@AfterClass(alwaysRun = true)
	private void testmethod() throws IOException {
		Screenshot scr=new Screenshot(driver);
		scr.getScreenshot();
		driver.quit();
	}
	
	private void launchBrowser() {
		ChromeOptions chromeOptions = new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver");
		chromeOptions.addArguments("--headless");
		chromeOptions.addArguments("--window-size=1920,1080");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--disable-extensions");
		chromeOptions.addArguments("--no-sandbox");
		driver = new ChromeDriver(chromeOptions);
		System.out.println("Browser opened");
		driver.get(properties.get("url"));
		System.out.println("Navigated to - "+properties.get("url"));
		//driver.navigate().to(properties.get("url"));
	}
	
	TestUtils testutils;
	
	@Test
	public void testSeo() throws InterruptedException {
		System.out.println("Starting test Method");
		testutils= new TestUtils(driver);
		System.out.println("Login starting");
		testutils.login(properties.get("email"), properties.get("password"), properties.get("url"));
		System.out.println("Logged out. Starting actual test");
		testutils.addSeoHomePage();
	}

}
